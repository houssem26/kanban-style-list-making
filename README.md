A Frontend only Kanban Style list maker,

Possibility to Add/Delete lists,
Cards for each List and todo items to check for each card,

Stack :

- React
- Redux
- Aphrodite
- uniqid library to generate identifier for each List/card
- Webpack for build and serving


getting started

- npm install
- npm start
- then navigate to 
- loalhost:8080
